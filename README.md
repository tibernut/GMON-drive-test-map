### DriveTest Map

This script will allow you to turn csv data taken from GMON and turn it into Google Earth kmls.  It helps you visualize a cell's coverage and can help spot swapped cells and missing handovers.

<a href="https://play.google.com/store/apps/details?id=de.carknue.gmon2&hl=en_US">G-MoN</a> is an Android app available for free on the Google Play store.  To generate files to process simply go into the apps settings and have it generate csv files.  Then hit the log button.  Run the generated .csv file through this script.

There are three options in the script.
* Analyze - displays some generalized information about the file that was loaded.  This is helpful when you have many drivetests files and are trying to find a specific one.
* Cell ID Map - Creates a Google Earth KML coloring each measurement based on which cell the UE was attached to.
* Signal Map - Creates a Google Earth KML coloring each measurement based on the measurement specified in prefs.json.

There are also some preferences you can set from file>preferences or from prefs.json directly.
* \# of Map Colors for Signal - this allows you to increase/decrease the number of different colors are used to display different Signal measurements.
* Meas. used for Signal Map - this changes the signal used while creating the signal strength map.  Valid options are SiNR, RSL, RSRP, RSRQ.  Please note that SiNR, RSRP, and RSRQ are LTE specific. If you choose one of these UMTS, GSM, and CDMA measurements will not be plotted.  Please use RSL if you are measuring one of those technologies.
* Cell Dictionary Path - You can have the script translate the cell IDs for you if you provide a csv file.  In the file simply add the CIDs and the site names.  IE 99999, Test_Cell_1
* Save Path - Where you want the script to put the generated KML files.

There is a binary provided for Linux systems.  At some point in the future I will generate one for Windows.  To use it you should just be able to download the linux_binaries.zip file, unzip it, and run the DrivetestMap binary.

By running driveTestProcess you will have access to a CLI version of the utility. Use --help to understand the command line switches.

Step by step guide:
* Download Gmon from the <a href="https://play.google.com/store/apps/details?id=de.carknue.gmon2&hl=en_US">Google Play Store</a>.

* The first time you run the program you must set the logging type to CSV in order to generate files DriveTest Map can use.
  * Click the 3 verticle dots in the top right of the screen and select 'Settings'
  * Then select '2G/3G/4G'
  * Scroll down until you see 'RX Log File Type' and select it<br />
  <img src="http://sanitizedinput.com/static/images/2G-3G-4G.png" width="250" /><br /><br />
  * Now choose 'CSV' <br />
  <img src="http://sanitizedinput.com/static/images/RXL-Log-File-Type.png" width="250" /><br /><br />
* To start logging cell data with the GMON application touch the circle icon in the top right of the app on the main screen.  When you are finished with your drive test just press it again to stop logging.
* The files will by default stored on your android device on the internal memory in a folder called 'gmon.'  The files you are looking for will be .txt files.
* Transfer those files to your PC and start the DriveTest Map program.  Open one of the files by clicking file>open<br />
![Script GUI image](http://sanitizedinput.com/static/images/drivetestmap_GUI.png)
* Then click either 'Analyze', 'Cell ID Map', or 'Signal Map' at the bottom of the program window.
* This will generate .kml files that can be opened with <a href="https://www.google.com/earth/versions/">Google Earth Desktop</a>.<br />
![KML example image](http://sanitizedinput.com/static/images/phone.png)
* By default DriveTest Map will save the generated kml files into the same folder as the script.  If you want to change the default location, you can by modifying the preferences. (file>preferences)


    DriveTest Map is a free tool that creates kml files from GMON generated data

    Copyright (C) 2019  Christian Jones

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
